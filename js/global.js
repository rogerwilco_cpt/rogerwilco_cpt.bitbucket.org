// ROGERWILCO FRAMEWORK : Anton Boshoff //
 
// ==== INDEX ==== //
// Global Variables
// Initiate Accordion
// Initiate Tabber
// Equal Heights
// Disable Middle Mouse Click
// Navigation Stick
// Navigation Scroll to ID


// ==== NAMESPACE ==== //
window.app = {}


// ==== TRIGGER ON DOCUMENT READY ==== //
jQuery(function($) {

	try {

		app.go.globalVar();
		app.go.initAccordion();
		app.go.initTabber();
		app.go.disableMiddleMouse();
		app.go.navStick();
		app.go.navNavigate();

		// ==== TRIGGER ON RESIZE ( DEBOUNCE ) ==== //
		// function updateResizeDebounce() {

		// }

		// var lazyLayout = _.debounce(updateResizeDebounce, 10);
		// app.go.win.resize(lazyLayout);

		// ==== TRIGGER ON RESIZE ( THROTTLE ) ==== //
		function updateResizeThrottle() {

			// dynamic values
			app.go.winWidth = app.go.win.width();

			if ( app.go.winWidth < 960 ) {
				app.go.isMobile = true;
			} else {
				app.go.isMobile = false;
			}

			app.go.equalHeights();

		}

		var rThrottled = _.throttle(updateResizeThrottle, 10);
		app.go.win.resize(rThrottled);

		// ==== TRIGGER ON SCROLL ( THROTTLE ) ==== //
		function updateScrollThrottle() {

			// dynamic values
			app.go.winTop = app.go.win.scrollTop();

			app.go.navStick();

		}

		var sThrottled = _.throttle(updateScrollThrottle, 10);
		app.go.win.scroll(sThrottled);

	} catch(err) {

		console.log(err);

	}

});

// ==== TRIGGER AFTER PAGE LOAD ==== //
jQuery(window).load(function(){
	
	app.go.equalHeights();

});


// ==== FUNCTIONS ==== //
app.go = {

	// Global Variables
	globalVar: function() {

		// variables
		app.go.win = jQuery(window);
		app.go.body = jQuery('body');
		app.go.nav = jQuery('nav#nav');

		// dynamic values
		app.go.winHeight = app.go.win.height();
		app.go.winWidth = app.go.win.width();
		app.go.winTop = app.go.win.scrollTop();

		if ( app.go.winWidth < 960 ) {
			app.go.isMobile = true;
		} else {
			app.go.isMobile = false;
		}

	},

	// Initiate Accordion
	initAccordion: function(){

		// variables
    	var indi = false,
    		accordion = jQuery('.accordion');
    		expand = jQuery('.is-expanded');

    	// if items independant from siblings
    	if ( accordion.hasClass('is-independent') ) {
    		indi = true;
    	}

    	// fade/hide items on initial load
    	jQuery('.itemContents')
      		.css('opacity', 0)
      		.each(function(){
        		var getHeight = jQuery(this).find('.itemContents').height();
        		jQuery(this).find('.itemContents').css('height', getHeight);
      		});
		accordion.find('.itemContents').hide();

		// show items with .is-expanded class
		expand.find('.itemContents')
  			.show()
  			.css('opacity', 1);
		expand.find('.icon').html('<i class="fa fa-chevron-up"></i>');

		// on click
		jQuery('.bar').on('click', function(){
  			var self = jQuery(this);

  			// fade/hide all items
  			if ( indi ) {
  				// do nothing
  			} else {
  				self.parent().parent().children('.is-expanded').find('.itemContents').animate({ opacity: 0 }, function(){
		        	jQuery(this).slideUp(function(){
		          		jQuery(this).parent().removeClass('is-expanded');
		        	});
		      	});
  			}

	      	// expand/show if not already showing
  			if ( self.parent().hasClass('is-expanded') ) {
  				if ( indi ) {
  					self.siblings('.itemContents').animate({ opacity: 0 }, function(){
  						jQuery(this).slideUp();
  						jQuery(this).closest('.accordion>li').removeClass('is-expanded');
	    			});
  				} else {
  					// do nothing
  				}
  			} else {
    			self.siblings('.itemContents').slideDown(function(){
      				jQuery(this).animate({ opacity: 1 });
      				jQuery(this).closest('.accordion>li').addClass('is-expanded');
    			});
  			}
		});

  	},

  	// Initiate Tabber
  	initTabber: function(){

	    jQuery('ul.tabber>li').on('click', function(){
	      	var self = jQuery(this),
	        	dataContent = self.attr('data-contents'),
	          	tabberContent = self.parent('ul').siblings('.tabberContent');
	      	
	      	if (self.parent().hasClass('open')) {
	        	// do nothing
	      	} else {
	        	self.siblings().removeClass('open');
	        	self.addClass('open');

	        	tabberContent.children('li[data-tab!='+dataContent+']').hide();
	        	tabberContent.children('li[data-tab='+dataContent+']').show();
	      	}
	    });

	},

	// Equal Heights
	equalHeights: function() {

		var row = jQuery('.equalize');

		row.each(function() {

			var item = jQuery(this).find('.rw-col');

			if ( app.go.winWidth > 768 ) {

				var tallest = 0;

				item.height('auto');

				item.each(function() {
					var iHeight = jQuery(this).height();

					tallest = tallest > iHeight ? tallest : iHeight;
				});

				item.height(tallest);
			} else {
				item.height('auto');
			}

		});

		jQuery('.pager').find('a').on('click', function() {
			setTimeout(function() {
				app.go.win.trigger('resize');
			}, 600);
		});

	},

	// Disable Middle Mouse Click
	disableMiddleMouse: function() {

		app.go.body.on('mousedown', function(e) {

			if ( e.which == 2 ) {
				e.preventDefault();
			}

		});

	},

	// Navigation Stick
	navStick: function() {

		if ( app.go.nav.hasClass('stick') ) {
			var navTop = 134;
		} else {
			var navTop = app.go.nav.offset().top - 10;
		}

		if ( app.go.winTop > navTop ) {
			app.go.nav.addClass('stick');
		} else {
			app.go.nav.removeClass('stick');
		}

	},

	// Navigation Scroll to ID
	navNavigate: function() {

		var link = app.go.nav.find('a'),
			article = jQuery('article.article'),
			browser = jQuery('html, body');

		link.on('click', function(e) {

			e.preventDefault();

			var self = jQuery(this),
				id = self.attr('data-id');

			browser.animate({
				scrollTop: jQuery('#' + id).offset().top
			}, 1000);

		});

	}

}